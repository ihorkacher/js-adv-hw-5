export {
    tweetsElementHTML, usersElementHTML, usersURL, tweetsURL,
};

const tweetsElementHTML = document.querySelector('.tweets');
const usersElementHTML = document.querySelector('.users');

const usersURL = 'https://ajax.test-danit.com/api/json/users';
const tweetsURL = 'https://ajax.test-danit.com/api/json/posts';