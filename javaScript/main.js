import { tweetsElementHTML, usersElementHTML, } from "./constants/constants.js";
import { TweetPage } from "./classes/TweetPage.js";

new TweetPage().renderContent()
    .then(object => {
        tweetsElementHTML.append(...object.tweets);
        usersElementHTML.append(...object.users);
    });