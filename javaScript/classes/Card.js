import { createElementHTML } from "../functions/functions.js";
import { Request } from "./Request.js";
import { tweetsURL } from "../constants/constants.js";

export class Card {
    constructor(obj = '') {
        this.obj = obj;
    }
    createPostCard(){
        const { name, email, title, body, tweetId } = this.obj;
        const divWrapper = createElementHTML('div', 'tweet__card', '');
        const authorName = createElementHTML('span', 'card__name', `${name}  - `);
        const authorEmail = createElementHTML('span', 'card__email', email);
        const authorTitle = createElementHTML('h2', 'card__title', title);
        const authorBody = createElementHTML('p', 'card__text', body);
        const postDeleteBtn = createElementHTML('span', 'card__delete', '+');
        postDeleteBtn.id = tweetId;
        this.deletePostCard(postDeleteBtn, tweetId, divWrapper);
        divWrapper.append(authorName, authorEmail, authorTitle, authorBody, postDeleteBtn, );

        return divWrapper;
    }
    createUserCard(usersArr){
        return usersArr.map(user => {
            const { name, email } = user;
            const divWrapper = createElementHTML('div', 'user__card', '');
            const authorName = createElementHTML('div', 'card__name', name);
            const authorEmail = createElementHTML('div', 'card__email', email);
            divWrapper.append(authorName, authorEmail);

            return divWrapper;
        })
    }
    deletePostCard(postDeleteBtn, id, card){
        return postDeleteBtn.addEventListener('click', () => {
            return new Request(`${tweetsURL}/${id}`).delete()
                .then((data) => {
                    console.log('Item deleted successfully:', data);
                    card.remove();
                })
                .catch((error) => {
                    console.error('Failed to delete item:', error);
                });
        })
    }

}