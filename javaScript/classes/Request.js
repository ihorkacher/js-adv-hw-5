export class Request {
    constructor(url) {
        this.url = url;
    }

    get() {
        return fetch(this.url)
            .then((response) => {
                if (!response.ok) {
                    throw new Error(`HTTP error! status: ${response.status}`);
                }
                return response.json();
            })
            .catch((error) => {
                console.error(error);
                throw error;
            });
    }
    delete() {
        const options = { method: 'DELETE' };
        return fetch(this.url, options)
            .then((response) => {
                if (!response.ok) {
                    throw new Error(`HTTP error! status: ${response.status}`);
                }
                return response;
            })
            .catch((error) => {
                console.error(error);
                throw error;
            });
    }
}
