import { Request } from "./Request.js";
import { Card } from "./Card.js";
import {tweetsURL, usersURL, tweetsElementHTML } from "../constants/constants.js";

export class TweetPage {
    getPromise(url) {
        return new Request(url).get().then(data => {
            return data;
        });
    }
    renderContent() {
        return Promise.all([this.getPromise(usersURL), this.getPromise(tweetsURL)])
            .then(data => {
                const users = data[0];
                const tweets = data[1];

                const usersCards = new Card().createUserCard(users);
                const tweetsCards = tweets.map( ({ id, userId, title, body }) => {
                    const user = users.find(user => user.id === userId);
                    const { name, email } = user;

                    return new Card({
                        name: name,
                        email: email,
                        title: title,
                        body: body,
                        tweetId: id,
                        userId: userId
                    }).createPostCard();
                })

                return {
                    users: usersCards,
                    tweets: tweetsCards
                }
            })
            .catch(() => {
                tweetsElementHTML.innerHTML = '<div class="Oops">Oops, something went wrong!</div>';
                tweetsElementHTML.style.textAlign = 'center';
                tweetsElementHTML.style.marginTop = '345px';
                tweetsElementHTML.style.fontSize = '35px';
                tweetsElementHTML.style.color = 'red';
                setTimeout(() => {
                    document.querySelector('.Oops').remove();
                }, 5000)
            })
            .finally(() => {
                document.querySelector('.post-loader').remove();
            })
    }
}